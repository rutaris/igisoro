
# comment

# simulation of igisoro


import random
import os
import copy

LEN_BOARD = 8
N_STONES_0 = 4

random.seed(7)

class Board:
	''' Create an Igisoro board class. 
	'''
	def __init__ (self, nstones_0 = N_STONES_0, len_board = LEN_BOARD):
		''' 
			Initialize a igisoro board. 
			Board is represented by a dictionary with keys 0, and 1.
			each value holds an array of len_board (normally 16)
			INPUT:
				- n_stones: initial number of stones in a pit. Normally 4
				- len_board: length of board. Normally 8
		'''
		self.nstones_0 = nstones_0
		self.len_board = len_board

		player_rows = [nstones_0 for i in range(len_board)] + \
						[0 for i in range(len_board)]

		self.board = {0: player_rows, 1: copy.deepcopy(player_rows)}

	def show(self, marks= None):
		'''
			:param board: a dict of player rows {0:[.],1:[.]}
			:param marks: a dict of player marks {0:[.],1:[.]}, 
						default value is None
			:return: 
		'''

		# A bit of logic to allow this function to take either an empty input
		# or a short-hand version of marks. So for example you only specify
		# the marks for player 1, and the function will understand that 
		# for player 0 there aren't any marks, and vice versa. 
		if marks is None:
			marks = {0:[], 1:[]}
		else:
			if 0 not in marks:
				marks[0] = []
			if 1 not in marks:
				marks[1] = []

		# turn the ints in boards into strings here, before transforming
		# it into pattern
		board_arr_str = []
		for player_id in range(2):
			player_rows_str = [str(self.board[player_id][i])+'*' \
								   if i in marks[player_id]\
								   else str(self.board[player_id][i])+' ' \
							   for i in range(self.len_board*2)]
			board_arr_str.append(player_rows_str)

		midline = '+====' * self.len_board + '+\n'
		line = '+----' * self.len_board + '+\n'
		pattern = '|{:>4s}' * self.len_board + '|\n'

		p0_back = board_arr_str[0][-(self.len_board):][::-1]
		p0_front = board_arr_str[0][:(self.len_board)]
		p1_front = board_arr_str[1][:(self.len_board)][::-1]
		p1_back = board_arr_str[1][-(self.len_board):]

		players_rows = {0: [p0_back, p0_front], 1: [p1_front, p1_back]}
		board_str = line

		for player_id in players_rows:
			for row in players_rows[player_id]:
				row_str = pattern.format(*row)
				board_str += row_str
			if player_id == 0:
				board_str += midline

		board_str += line
		print(board_str)

	def get_board(self):
		return self.board

	def get_len_board(self):
		return len_board

	def make_run(self, player_id, pit_id, verbose=0):
		'''
			Given a player id and a pit id, check that the position is 
			valid and make a move, without captures (for now)
		'''

		if self.board[player_id][pit_id] <= 1:
			raise ValueError('pit_id value fed to make_run is not valid.')

		in_hand = self.board[player_id][pit_id]
		if verbose:
			print("make_run: Player %d started move from pit %d"% \
					(player_id, pit_id))

		while in_hand > 1:
			self.board[player_id][pit_id] = 0
			for i in range(in_hand):
				pit_id = (pit_id + 1) % (self.len_board * 2)
				tmp = self.board[player_id][pit_id]
				self.board[player_id][pit_id] = tmp+1
			if verbose:
				self.show({player_id:[pit_id]})
			in_hand = self.board[player_id][pit_id]        

	def _randomize_one_side(self, player_id, n_moves, verbose=0):
		'''
			Given a player id, make n_moves randomly from a reset board, 
			or a board with n_stones > 16, otherwise it may be a lost game, 
			i.e one player has max stones per pit = 1: lost. 
			For each moves, select a random pit with at least 2 stones and 
			start a move (which may include many pickups.)
		'''

		if verbose:
			print("_randomize_one_side: Player %d, %d moves "% \
					(player_id, n_moves))

		for i in range(n_moves):
			valid_pits = [j for j in range(self.len_board*2) \
							if self.board[player_id][j] >= 2]
			chosen_pit_id = random.randint(0, len(valid_pits)-1)
			chosen_pit = valid_pits[chosen_pit_id]

			# if verbose:
			#     print "_randomize_one_side %d: playerID %d, pitID %d (%d)" % \
			#         (i, player_id, chosen_pit, \
			#         self.board[player_id][chosen_pit])
			self.make_run(player_id, chosen_pit, verbose=0)
		
		if verbose:
			self.show()

	def randomize_both_side(self, n_moves, verbose=0):
		'''
			Randomize both side of the board, by calling the method
			make_run once for each player
		'''

		for player_id in self.board:
			self._randomize_one_side(player_id, n_moves,0)
		
		if verbose:
			print("Randomized board:")
			self.show()

	def is_game_completed(self):
		'''
			Return 1 is a game is won, else return 0
		'''
		if max(self.board[0]) == 1:
			print("Player 1 has won the Game")
			self.show()
			return True
		elif max(self.board[1]) == 1:
			print("Player 0 has won the Game")
			self.show()
			return True
		else:
			return False
		
	def make_move(self, player_id, pit_id, verbose=True):
		'''
			For a given player, and pit_id, make a run. 
			Here, move is defined as Run + Capture(s)
		'''
		# save original pit_id to be returned later.
		pit_id_0 = pit_id

		if self.board[player_id][pit_id] < 2:
			raise ValueError('pit_id %d, player_id %d has less than 2 stones' %\
							(pit_id, player_id))

		in_hand = self.board[player_id][pit_id]
		total_captured = 0
		n_pickups = 0

		future_loc = (pit_id + in_hand) % (self.len_board*2)
		if verbose:
			print("Player {:2d} is starting at {:2d}, with {:2d} stones in hand."\
				" (=> {:2d} [{:2d}])".format(player_id, pit_id, in_hand,\
					future_loc, self.board[player_id][future_loc]))
		# while player has stones in hand and game is not complete
		# make a run
		while (in_hand > 1) and (not self.is_game_completed()):
			n_pickups += 1

			self.board[player_id][pit_id] = 0
			# run from start to interm. stop. 
			for i in range(in_hand):
				pit_id = (pit_id + 1) % (self.len_board * 2)
				tmp = self.board[player_id][pit_id]
				self.board[player_id][pit_id] = tmp+1
			
			# at this point pit_id is the interm. stop. 
			# if pit is in the front and has more than 2 stones.
			# if yes, 
			#    - check if opp has enough stones to capture
			#        if yes:
			#            in hand = capture
			#            set opp pit values to 0
			#            return to previous pit.
			#        if not:
			#            in hand = current pit_id content
			# if not, 
			#    - in hand = current pit_id content.

			# run ended in front row, and about to go again
			if (pit_id < self.len_board) and \
				(self.board[player_id][pit_id] >= 2): 
				opp_pit_front = (self.len_board - 1) - pit_id
				opp_pit_back = ((self.len_board * 2) - 1) - opp_pit_front

				opp_pit_front_stones = self.board[not player_id][opp_pit_front]
				opp_pit_back_stones = self.board[not player_id][opp_pit_back]

				# a capture can happen
				if (opp_pit_front_stones > 0) and (opp_pit_back_stones > 0):
					captured_stones = opp_pit_front_stones + \
										opp_pit_back_stones
					total_captured += captured_stones

					self.board[not player_id][opp_pit_front] = 0
					self.board[not player_id][opp_pit_back] = 0

					in_hand_tmp = in_hand
					pit_id_tmp = pit_id

					in_hand = captured_stones
					pit_id = (pit_id - in_hand_tmp) % (self.len_board*2)

					future_loc = (pit_id + in_hand) % (self.len_board*2)

					if verbose:
						print("   Player {:2d} is at {:2d}, captured {:2d} stones,"\
							" going back to {:2d} (=> {:2d} [{:2d}])".format(\
								player_id, pit_id_tmp,captured_stones, pit_id,\
								future_loc, self.board[player_id][future_loc]))

					# self.show({not player_id:[opp_pit_front, opp_pit_back], \
					#             player_id:[pit_id_tmp]})

				# can't capture
				else:
					in_hand = self.board[player_id][pit_id]
					future_loc = (pit_id + in_hand) % (self.len_board*2)

					if verbose:
						print("   Player {:2d} is at {:2d}, can't capture, has "\
							"{:2d} stones in hand (=> {:2d} [{:2d}])".format(\
							player_id, pit_id, in_hand, future_loc,\
							self.board[player_id][future_loc]))

					# self.show({not player_id:[opp_pit_front, opp_pit_back], \
					#             player_id:[pit_id]})

			# run ended in the back row or front row but with one stone.
			else:
				in_hand = self.board[player_id][pit_id]
				future_loc = (pit_id + in_hand) % (self.len_board*2)

				# front row but with one stone.
				if (pit_id < self.len_board): 

					if verbose:
						print("   Player {:2d} is at {:2d}, Run is over.".format(\
							player_id, pit_id))
					# self.show({player_id:[pit_id]})
				else: 
					# self.show({player_id:[pit_id]})
					if verbose:
						print("   Player {:2d} is at {:2d}, can't capture, has " \
						"{:2d} stones in hand (=> {:2d} [{:2d}])".format(\
							player_id, pit_id, in_hand, future_loc,\
							self.board[player_id][future_loc]))

		return (pit_id_0, total_captured, n_pickups)

class GameEngine(Board):
	def __init__(self, n_games = 1):
		# board, n_games
		self.board = Board()
		self.n_games = n_games
		# create players
		self.players = [Player(i) for i in range(2)]

	def simulate_one_game(self):
		player_turn_id = 0
		turn_nbr = 0

		while self.players[0].can_play(self.board) and \
			self.players[1].can_play(self.board):

			print("-"*90)
			turn_nbr += 1
			print("TURN # {:5d}".format(turn_nbr))

			player_move = self.players[player_turn_id].get_most_capture_move(self.board)
			self.board.make_move(player_turn_id, player_move[0])

			player_turn_id = (player_turn_id + 1) % 2

		print("done")
		if not self.players[0].can_play(b):
			print(self.players[0].can_play(b), self.players[1].can_play(b))
			print("Player 1 has won.")
			print("nbr of turns: {:d}".format(turn_nbr))
		if not self.players[1].can_play(b):
			print(self.players[0].can_play(b), self.players[1].can_play(b))
			print("Player 0 has won.")
			print("nbr of turns: {:d}".format(turn_nbr))
		else:
			print(self.players[0].can_play(b), self.players[1].can_play(b))
			print("Tie ??")


class Player:
	def __init__(self, player_id):
		self.player_id = player_id

	def get_most_capture_move(self, b):
		""" Given a board b, return the move that will lead to the most capture.
		"""

		get_valid_pits = lambda pl_id: [i for i in range(len(b.board[pl_id]))\
										if b.board[pl_id][i] >= 2]
		pit_0 = get_valid_pits(self.player_id)
		move_capture_pickups = []

		for pit_id in pit_0:
			b_tmp = copy.deepcopy(b)
			tmp_result = b_tmp.make_move(self.player_id, pit_id, 0)
			move_capture_pickups.append(tmp_result)
		move_capture_pickups = sorted(move_capture_pickups, \
											key=lambda x: x[1], reverse=1)
		most_capture_move = move_capture_pickups[0]

		return most_capture_move

	def can_play(self, b):
		if max(b.board[self.player_id]) < 2:
			return False
		else:
			return True

b = Board()
# b.board = {0:[0,2,2,3,0,1,0,6,1,5,1,2,1,3,5,2],\
#              1:[6,0,1,0,3,3,3,2,0,2,5,2,3,0,0,0]}
b.show()
g = GameEngine()











